/**
 * Copyright 2018-2019 CS Systèmes d'Information
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * A VizTool permitting to list the funcIDs and to grant the user options to focus, and plot.
 *
 * @constructor
 * @param {string} container - the ID of the injected div
 * @param {Object} data - the data used in the visualization
 *                  Contains: {Array} a list of couples {"tsuid":"XX","funcId":"XX"}
 * @param {Object} callbacks - Dictionary of the callbacks available
 *                  Contains: engine - the parent engine instance
 */

class TsTable extends VizTool {
    constructor(container, data, callbacks) {
        super(container, data, callbacks);

        // Alphabetical natural sorting
        let cloneData = JSON.parse(JSON.stringify(data));
        this.sortedData = cloneData.sort((a, b) => a.funcId.localeCompare(b.funcId, undefined, {
            numeric: true,
            sensitivity: 'base'
        }));
        this.name = "TsTable";
        // List of the selected TS to be displayed to the next viztool
        this.selected = [];
        // html tag where the data will be displayed
        this.dataContainer = $('#' + container);
        // Number of data displayed in one page
        this.pageSize = 15;
        // Original data and its metadatas
        this.md = this.sortedData;
        // Sorted original data
        this.initialData = this.sortedData;
        // Current page
        this.currPage = 1;
    }

    /**
     * Display function: render the table
     */
    display() {

        const self = this;

        // Generation of the top options bar
        self.topmenu();

        // Generation of the table and it's pagination
        self.paginate(self.md);

        // Seting the pagesize dropdown to the right index
        if (self.pageSize != 15) {
            $('#Page_size_dropdown option').filter(function () {
                return $(this).html() == self.pageSize;
            }).attr('selected', 'selected');
        }



        // Handling of the top menu buttons clicks
        self.addButtonsClickEvents();

    }

    /**
     * Rendering of the top menu including the searchbar and the buttons
     */
    topmenu() {
        // Top menu
        let html = '' +
            '<div class="row" style="margin-left: auto; margin-right: 10px;">' +
            '   <div class="input-group col-lg-5" style="margin-left: auto; margin-right: 10px; float: left;">' +
            '   <input class="form-control" id="search_input" type="search" placeholder="Search FuncId(s)" aria-label="Search">' +
            '   <span class="input-group-addon" id="search_btn"><i class="glyphicon glyphicon-search"></i></span>' +
            '   </div>' +
            '   <div class="btn-group" role="group">' +
            '     <button type="button" class="btn btn-default" id="select_all" style="margin-right: 0">' +
            '          <span class="glyphicon glyphicon-check" aria-hidden="true"></span> Select all</button>' +
            '     <button type="button" class="btn btn-default" id="unselect_all" >' +
            '       <span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span> Unselect all</button>' +
            '   </div>' +
            '   <button type="button" class="btn btn-default " id="visualize_selected">' +
            '           <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Visualize selected</button>' +
            '   <button type="button" class="btn btn-default " id="filter_selected">' +
            '           <span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> Filter selected</button>' +
            '</div>';

        this.dataContainer.append(html);
    }

    /**
     * Used to update the information text of the page when it changes
     */
    updateInfoPagination() {
        let filtered = this.initialData.length - this.sortedData.length;

        let html = 'Showing ' + this.sortedData.length;
        if (filtered != 0) {
            html += ' matching TS <b>over ' + this.initialData.length + ' total. </b>';
        } else {
            html += ' ts. ';
        }
        html += this.selected.length + ' selected.';
        $('.info_pagination').html(html);
    }

    /**
     * Rendering of the table and the pagination
     */
    paginate(meta) {

        const self = this;
        self.dataContainer.append('<div class="DivWithScroll" id="paginated-table"></div> <div class="info_pagination"></div> ' +
            // Dropdown for selecting the page size
            '  <select class="form-control col-lg-2" id="Page_size_dropdown" style="width: 10%; margin-right: 10px; float: right"> ' +
            '  Select the page size ' +
            '     <optgroup label="Select the page size"> ' +
            '         <option selected hidden> Select the page size</option>' +
            '         <option >5</option> ' +
            '         <option >10</option> ' +
            '         <option >15</option> ' +
            '         <option >20</option> ' +
            '         <option >25</option> ' +
            '         <option >50</option> ' +
            '         <option >100</option> ' +
            '     </optgroup>' +
            '    </select> ' +
            '  <br>' +
            ' <div id="pagination-container"></div>');

        $('#pagination-container').pagination({
            dataSource: meta,
            pageSize: this.pageSize,
            pageNumber: 1,
            pageRange: 10,
            showGoInput: true,
            showGoButton: true,
            callback: function (md, pagination) {

                self.currPage = pagination.pageNumber;
                self.fetchData(self.currPage).then((fetched_md) => {
                    let meta_temp = fetched_md.slice((self.currPage - 1) * this.pageSize, self.currPage * this.pageSize);
                    var html = simpleTemplating(meta_temp);
                    $('#paginated-table').html(html);
                    // Handling of the clicks on the list
                    self.addListClickEvents();
                    self.updateInfoPagination();
                });
            }
        });

        function simpleTemplating(md) {

            var html = '        <table class="table table-hover">' +
                '                  <thead>' +
                '                    <tr>' +
                '                      <th scope="col">Selected</th>' +
                '                      <th scope="col">FuncId(s)</th>' +
                '                      <th scope="col">Start date</th>' +
                '                      <th scope="col">End date</th>' +
                '                      <th scope="col">Nb points</th>' +
                '                      <th scope="col"></th>' +
                '                    </tr>' +
                '                  </thead>' +
                '                  <tbody>';

            $.each(md, function (index, item) {
                html += '<tr class="md_line" id="line_' + item.tsuid + '" ';
                if (self.selected.includes(item.tsuid)) {
                    html += 'style="background-color: #B4D5FF"';
                }
                html += '>';
                html += '<td> <input type="checkbox" id="' + item.tsuid + '" class="ts_input" ';
                if (self.selected.includes(item.tsuid)) {
                    html += 'checked';
                }
                html += '> </td>';
                html += '<td>' + item.funcId + '</td>';
                html += '<td>' + item.start_date + '</td>';
                html += '<td>' + item.end_date + '</td>';
                // Adding tooltip if the number is wrapped
                if (item.nb_points > 999) {
                    html += '<td title="' + item.nb_points.replace(/\B(?=(\d{3})+(?!\d))/g, " ") + '">' + self.nFormatter(item.nb_points) + '</td>';
                } else {
                    html += '<td>' + self.nFormatter(item.nb_points) + '</td>';
                }
                html += '<td> <button class="btn btn-xs btn-secondary ts_button" id="btn_' + item.tsuid + '">' +
                    '<span class="glyphicon glyphicon-eye-open"></span> visualize</button> </td>';
                html += '</tr>';
            });

            html += '</tbody></table>';

            return html;
        }
    }

    /**
     * Activation of the event listeners and handling of the list buttons clicks
     */
    addListClickEvents() {
        const self = this;
        // Checkbox click
        $(".ts_input").click(function (event) {
            self.toggleSelected(event.target.id);
        });
        // TS row click
        $(".md_line").click(function (event) {
            if (event.target.tagName != "INPUT") {
                self.toggleSelected(event.target.parentElement.id.replace("line_", ""));
            }
        });
        // Visualize button click
        $(".ts_button").click(function (event) {
            let tsuid = event.target.id.replace("btn_", "");
            let ts = self.sortedData.find(item => item.tsuid == tsuid);
            self.addViz("Curve", [ts]);
        });
    }

    /**
     * Activation of the event listeners and handling of the buttons clicks
     */
    addButtonsClickEvents() {
        const self = this;
        // Visualize selected button click
        $("#visualize_selected").click(function () {
            if (self.selected.length > 0) {
                let ts = [];
                self.selected.forEach(selectedItem => {
                    ts.push(self.initialData.find(item => item.tsuid == selectedItem));
                });
                self.addViz("Curve", ts);
            } else {
                notify().error("Select at least one TS to display");
            }
        });
        // Select all button click
        $("#select_all").click(function () {
            self.selected = self.sortedData.map(item => item.tsuid);
            $(".ts_input").prop("checked", true);
            $(".md_line").css("background-color", "#B4D5FF");
            self.updateInfoPagination();
        });
        // Unselect all button click
        $("#unselect_all").click(function () {
            self.selected = [];
            $(".ts_input").prop("checked", false);
            $(".md_line").css("background-color", "");
            self.updateInfoPagination();
        });
        // Search all button click
        $("#search_btn").click(function () {
            self.searchFilter();
        });
        // pressing enter while searching
        $("#search_input").keypress(function (a) {
            if (a.keyCode == 13) {
                self.searchFilter();
            }
        });
        // On page-size change
        $('#Page_size_dropdown').change(function () {
            self.pageSize = $('#Page_size_dropdown :selected').text();

            let search = $('#search_input').val();
            // Destroying pagination instance
            $('#pagination-container').pagination('destroy');
            // Emptying the view
            self.dataContainer.html("");
            // Rebuilding the view
            self.display();
            // Filling the search bar again
            $('#search_input').val(search);
            self.updateInfoPagination();

        });
        // Filter selected button click
        $("#filter_selected").click(function () {

            // Filtering the data
            let selectedData = self.selected;
            let initData = self.initialData;
            let filteredData = initData.filter(data => selectedData.includes(data.tsuid));
            if (filteredData.length == 0) {
                notify().error("There is no selected TS ", "Nothing to show");
                return;
            }

            // Rebuilding the view
            self.sortedData = filteredData;
            self.md = filteredData;
            // Destroying pagination instance
            $('#pagination-container').pagination('destroy');
            // Emptying the view
            self.dataContainer.html("");
            // Rebuilding the view
            self.display();
            self.updateInfoPagination();


        });
    }

    /**
     * Add or remove a ts in the selected list
     * @param tsuid
     */
    toggleSelected(tsuid) {
        // If element exists, add or remove it from the selected list

        if (this.selected.includes(tsuid)) {
            // Remove
            this.selected.splice(this.selected.indexOf(tsuid), 1);
            $('#' + tsuid).prop('checked', false);
            $("#line_" + tsuid).css("background-color", "");

        } else {
            //Add
            this.selected.push(tsuid);
            $('#' + tsuid).prop('checked', true);
            $("#line_" + tsuid).css("background-color", "#B4D5FF");
        }


        this.updateInfoPagination();
    }

    /**
     * Used to display a convenient number
     * @param num
     * @param digits
     * @returns {string}
     */
    nFormatter(num, digits = 1) {
        var si = [
            {value: 1, symbol: ""},
            {value: 1E3, symbol: "k"},
            {value: 1E6, symbol: "M"},
            {value: 1E9, symbol: "G"},
            {value: 1E12, symbol: "T"},
            {value: 1E15, symbol: "P"},
            {value: 1E18, symbol: "E"}
        ];
        var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
        var i;
        for (i = si.length - 1; i > 0; i--) {
            if (num >= si[i].value) {
                break;
            }
        }
        return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
    }

    /**
     * Retrieve the data and add it to the md global variable. Also resolve this.md data
     * @param currPage
     * @returns {Promise<array>}
     */
    fetchData(currPage) {
        const self = this;

        return new Promise(function (resolve) {

            // Is the data already loaded ?
            if (self.isDataLoaded(currPage)) {
                resolve(self.md);
            } else {
                // Fetching the data
                ikats.api.md.read({
                    async: true,
                    ts_list: self.sortedData.filter(d => d.tsuid).slice((currPage - 1) * self.pageSize, currPage * self.pageSize),
                    success: function (r) {
                        self.md.forEach((item, index) => {

                            self.md[index].start_date = r.data.find(i => (i.name == 'ikats_start_date') && (i.tsuid == item.tsuid)) ?
                                self.formatDate(r.data.find(i => (i.name == 'ikats_start_date') && (i.tsuid == item.tsuid)).value)
                                : self.md[index].start_date;

                            self.md[index].end_date = r.data.find(i => (i.name == 'ikats_end_date') && (i.tsuid == item.tsuid)) ?
                                self.formatDate(r.data.find(i => (i.name == 'ikats_end_date') && (i.tsuid == item.tsuid)).value)
                                : self.md[index].end_date;

                            self.md[index].nb_points = r.data.find(i => (i.name == 'qual_nb_points') && (i.tsuid == item.tsuid)) ?
                                r.data.find(i => (i.name == 'qual_nb_points') && (i.tsuid == item.tsuid)).value
                                : self.md[index].nb_points;

                        });
                    },
                    error: function (r) {
                        console.error("Impossible to get metadata List", r);
                    },
                    complete: function () {
                        resolve(self.md);
                    }
                });
            }
        });
    }

    /**
     * Used to format any date object using a specific pattern
     * @param {string|integer} d: timestamp date
     * @param {string} format: date pattern output
     * @returns {*}
     */
    formatDate(d, format = "YYYY-MM-DD HH:mm:ss.SSS") {
        let date;
        if (Number.isInteger(d)) {
            date = new Date(d);
        } else {
            date = new Date(parseInt(d));
        }

        return moment(date).utc().format(format).replace(".000", "");
    }

    /**
     * Checks if the page already has all the data needed to be displayed
     * @param pageNum
     * @returns {boolean}
     */
    isDataLoaded(pageNum) {
        for (let i = (pageNum - 1) * this.pageSize; i <= (pageNum * this.pageSize) - 1; i++) {
            if (this.md[i] && typeof (this.md[i].nb_points) == 'undefined' ||
                this.md[i] && typeof (this.md[i].start_date) == 'undefined' ||
                this.md[i] && typeof (this.md[i].end_date) == 'undefined') {
                return false;
            }
        }
        return true;
    }

    /**
     * Rebuilding the page to display matching searched ts
     */
    searchFilter() {

        // Filtering the data
        let search = $('#search_input').val();
        console.debug(search);
        let initData = this.initialData;
        let filteredData = initData.filter(data => data.funcId.toUpperCase().includes(search.toUpperCase()));
        if (filteredData.length == 0) {
            notify().error("There is no TS matching the search criteria: " + search, "Nothing to show");
            return;
        }

        // Rebuilding the view
        this.sortedData = filteredData;
        this.md = filteredData;
        // Destroying pagination instance
        $('#pagination-container').pagination('destroy');
        // Emptying the view
        this.dataContainer.html("");
        // Rebuilding the view
        this.display();
        // Filling the search bar again
        $('#search_input').val(search);
        this.updateInfoPagination();
    }

    /**
     * Redraw the table after being woken up
     */
    wakeUp() {
        this.display();
    }
}
